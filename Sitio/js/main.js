function mensajeAgregado(){
    // Muesta el resultado en la etiqueta
    document.getElementById("lbl_error").innerHTML = "Cliente Agregado Exitosamente";
   
}

function mostrarBuscar(){
    document.getElementById("panel").style.display="block";
}


function eliminarCliente(){
    document.getElementById("panel").style.display="none";
    document.getElementById("lbl_mensaje_cliente").innerHTML = "Cliente Eliminado Exitosamente";
}

function modificaCliente(){
    document.getElementById("txt_rut_cli").removeAttribute("readonly"  , false);
    document.getElementById("txt_nombre_cli").removeAttribute("readonly"  , false);
    document.getElementById("txt_fecha_nac").removeAttribute("readonly"  , false);
    document.getElementById("cmb_tipo_cliente").removeAttribute("disabled"  , true);
    document.getElementById("btn_eliminar_cli").style.display = "none";
    document.getElementById("btn_modificar_cli").style.display = "none";
    document.getElementById("btn_mod_cli").style.display = "block";

    
}

function modificarCliente(){
    document.getElementById("panel").style.display="none";
    document.getElementById("lbl_mensaje_cliente").innerHTML = "Cliente Modificado Exitosamente";

    
}

// Funciones para Libro //

function mensajeAgregadoLibro(){
    // Muesta el resultado en la etiqueta
    document.getElementById("lbl_error").innerHTML = "Libro Agregado Exitosamente";
   
}

function mostrarBuscarLibro(){
    document.getElementById("panel").style.display="block";
}


function eliminarLibro(){
    document.getElementById("panel").style.display="none";
    document.getElementById("lbl_mensaje_libro").innerHTML = "Libro Eliminado Exitosamente";
}

function modificaLibro(){
    document.getElementById("txt_titulo_libro").removeAttribute("readonly"  , false);
    document.getElementById("txt_autor_libro").removeAttribute("readonly"  , false);
    document.getElementById("txt_editorial").removeAttribute("readonly"  , false);
    document.getElementById("txt_nro_copias").removeAttribute("readonly"  , false);
    document.getElementById("cmb_tipo_libro").removeAttribute("disabled"  , true);
    document.getElementById("btn_modificar_libro").style.display = "none";
    document.getElementById("btn_eliminar_libro").style.display = "none";
    document.getElementById("btn_mod_libro").style.display = "block";

    
}
    
function modificarLibro(){
    document.getElementById("panel").style.display="none";
    document.getElementById("lbl_mensaje_libro").innerHTML = "Libro Modificado Exitosamente";

    
}